#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <signal.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <sys/mman.h>
#include <sys/wait.h>
#include <stdbool.h>

#define BUFFER_SIZE 100

struct Buffer{
  int count;
  int readPos;
  int writePos;
  int buffer[BUFFER_SIZE];
};


void sigusr1_handler(int sig){
  printf("SIGUSR1 Called!");
}


int main(){
  int produc, cons;

  // Put buffer in shared memory so both processes can use it
    struct Buffer* buf = mmap(NULL, sizeof(struct Buffer), PROT_READ | PROT_WRITE, 
                    MAP_SHARED | MAP_ANONYMOUS, -1, 0);


  signal(SIGUSR1, sigusr1_handler);

  produc = fork();
  cons = fork();

  if(produc == 0){
    while(1){
      printf("Enter a char: ");
      char c;
      scanf(" %c", &c);
   
      if(c == 'q'){
      
      }
      else {
        kill(cons, SIGUSR1);
      }
    }
  }

  if (cons == 0){
    while(1){
    
    }
  }
  return 0;
}

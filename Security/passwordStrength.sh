#!/bin/bash

x=$1

foobar_re='(?=.*[^a-z])(?=.*[[:digit:]])'

echo ${#x}

if [[ ${#x} == 8 ]] && [[ $x =~ $foobar_re ]] ; then
    echo 4 chars long!
fi

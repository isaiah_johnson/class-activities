/* Isaiah Johnson
*  CST-221
*  Bit Shifting
*/

#include <stdio.h>
 
void printBinary(int num){
  printf("%d to binary: ", num);
 
  // Declare ints for printing
  int c, bit, iteration;

  iteration = 0;
  for (c = 31; c >= 0; c--)
  {
      // Print a space every 8 bits to tidy up the output
      if(iteration % 8 == 0 && iteration != 0){
          printf(" ");
      }

      //Right shift the number c bits
      bit = num >> c;
 
      // If the bit is 1, print 1 otherwise print 0
      if (bit & 1)
        printf("1");
      else
        printf("0");


      iteration++;
  }
}


// Shifts bits to the left by a specified number
int bitShiftLeft(int leftNum, int num){
  printf("Shifting bits left by %d...\n", leftNum);
  return num << leftNum;

}

// Clear bits from specified bit to another specified bit
int clearBits(int startBit, int endBit, int num){
  int bit, c;
  
  printf("Clearing bits %d to %d...\n", startBit, endBit);
  
  // Iterates from endBit to startBit and clears bit
  for(c = endBit; c >= startBit; c--){
    // Create mask using AND, using not to invert the bits
    int mask = ~(1 << c);

    // Sets the bit at c to 0
    num &= mask;
  }

  return num;
}

// Adds hexadecimal to a number
int addHexadecimal(int hexadecimal, int num){
  printf("adding %08x to %d...\n", hexadecimal, num);
  return hexadecimal + num;
}

// Prints a number in hexadecimal format
void printHexadecimal(int num){
  printf("%d to hexadecimal: %08x", num, num);
}


// This function will output a newline a specified number of times
void newline(int lines){
  for(int i = 0; i < lines; i++){
    printf("\n");
  }
}

int main()
{
  int n;
 
  newline(2);
  // Take user input and save to n
  printf("Enter an integer (0 to 4095): ");
  scanf("%d", &n);
  newline(1);
  
  // Print binary format of number
  printBinary(n);
  newline(2);

  // Print hexadecimal format of number
  printHexadecimal(n);
  newline(2);

  // This will change the number when you shift to the left by 16 bits
  n = bitShiftLeft(16, n);
  printf("new num: %d\n", n);

  // Print binary format of number
  printBinary(n);
  newline(2);

  // This will clear the last 16 bits
  n = clearBits(0, 15, n);
  printf("new num: %d\n", n);

  // Print binary format of number
  printBinary(n);
  newline(2);

  // Finally, add 0x07FF to the number
  n = addHexadecimal(0x07FF, n);
  printf("new num: %d\n", n);
  newline(2);

  // Output the number in binary, hexadecimal, and decimal format
  printf("Final result: \n\n");
  printBinary(n);
  newline(1);
  printHexadecimal(n);
  newline(1);
  printf("decimal: %d", n);


  newline(4);
 
  return 0;
}

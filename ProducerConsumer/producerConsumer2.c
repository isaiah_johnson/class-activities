
// CPP code to create three child 
// process of a parent 
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <signal.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <sys/mman.h>
#include <sys/wait.h>
#include <stdbool.h>
  

#define BUFFER_SIZE 10

void sigusr1_handler();
int produce();
void consume();
void producer();
void consumer();
void put();
int get();


struct Buffer{
  int count;
  int readPos;
  int writePos;
  int buffer[BUFFER_SIZE];
};


// Driver code 
int main() 
{ 
    int producer, consumer; 

    // Put buffer in shared memory so both processes can use it
    struct Buffer* buf = mmap(NULL, sizeof(struct Buffer), PROT_READ | PROT_WRITE, 
                    MAP_SHARED | MAP_ANONYMOUS, -1, 0);

    // Store whether producer and consumer are awake or not in shared memory
    bool* producerAwake = mmap(NULL, sizeof(producerAwake), PROT_READ | PROT_WRITE, 
                    MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    bool* consumerAwake = mmap(NULL, sizeof(consumerAwake), PROT_READ | PROT_WRITE, 
                    MAP_SHARED | MAP_ANONYMOUS, -1, 0);

    // Set initial values for buffer
    buf->count = 0;
    buf->readPos = 0; 
    buf->writePos = 0;

    // Define our SIGUSR1 signal so the processes can use it
    signal(SIGUSR1, sigusr1_handler);
    
    // Fork our two processes
    producer = fork();
    consumer = fork();

    if(producer < 0 || consumer < 0){
        perror("forking error");
        exit(1);
    }

    // Producer process
    if(producer == 0){
	int i = 0;
	
	while(1){
	    printf("Buffer count(producer): %i\n", buf->count);
	    // If it's running producer is awake
	    *producerAwake = true;

	    if(buf->count >= BUFFER_SIZE){
		// Pause the producer if buffer is full
	        printf("Pausing producer... \n");
		*producerAwake = false;
	        pause();
	    }
            else if(buf->count >= 1){
		// Buffer is no longer empty, send wakeup to consumer if it's asleep
		if(*consumerAwake == false){
		    printf("Waking up consumer... \n");
	            kill(consumer, SIGUSR1);
		}  
	    }

	    // Produce a new number to put in buffer
	    i = produce(i);
	    // Put number in buffer
	    put(i, buf);


	    // Print the buffer for debug purposes
	    for(int i = 0; i < BUFFER_SIZE; i++){
	        printf("%i ", buf->buffer[i]);
	    }
            printf("\n");
	   
            sleep(1);
	}
    }

    // Consumer process
    if (consumer == 0) { 
	int i;
	while(1){
	    // If it's running, consumer is awake
	    *consumerAwake = true;

	    if(buf->count == 0){
		// Buffer is empty, pause the consumer, set awake to false
	        printf("Pausing consumer... \n");
		*consumerAwake = false;
	        pause();
	    }
            else if(buf->count < BUFFER_SIZE){
		// Buffer is no longer full, send a wakeup to producer if it's asleep
	        if(*producerAwake == false){
		    printf("Waking up producer... \n");
	            kill(producer, SIGUSR1);
		}  
		
	    }

	   // Buffer can be pulled from since it's not empty
	        i = get(buf);
	        consume(i);

	   // Print the buffer for debug purposes
	   for(int i = 0; i < BUFFER_SIZE; i++){
	       printf("%i ", buf->buffer[i]);
	   }
           printf("\n");

	   sleep(1);
        }
    } 
  
    return 0; 
} 


void sigusr1_handler(int sig){
    printf("SIGUSR1 called...\n");	
}

int produce(int i){
    return i+= 1;
}


void put(int i, struct Buffer* buf){
    printf("PRODUCER putting [%i] in index [%i]\n", i, buf->writePos); 
    // Set the buffer at the current write position to int passed to method
    buf->buffer[buf->writePos] = i;

    // Increment the count by 1 since we added a number
    buf->count += 1;

    // If the new writePos is greater than or equal to the buffer size, it is out of range
    if(buf->writePos + 1 >= BUFFER_SIZE){
	// Set writePos back to 0
        buf->writePos = 0;
    }
    else {
	// Increment writePos
        buf->writePos += 1;
    }
}


void consume(int i){
    printf("CONSUMING [%i]...\n", i);
}


int get(struct Buffer* buf) {
    int product;

    // While the index we are accessing is 0 (empty slot), skip to the next position
    while(buf->buffer[buf->readPos] == 0){
	if(buf->readPos + 1 >= BUFFER_SIZE){
            buf->readPos = 0;
        }
        else {
	    // Increment readPos by 1
            buf->readPos += 1;
        }
    }

    // Get the number from the buffer at the current read position
    product = buf->buffer[buf->readPos];
    // Set the new value to 0 to signify that this index is empty
    buf->buffer[buf->readPos] = 0;

    // We pulled out a number, so decrease count
    buf->count -= 1;

    // If the readPos is out of range if we increment, set it to 0
    if(buf->readPos + 1 >= BUFFER_SIZE){
        buf->readPos = 0;
    }
    else {
	// Increment readPos by 1
        buf->readPos += 1;
    }
    return product;
}


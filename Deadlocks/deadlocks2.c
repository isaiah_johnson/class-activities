// Isaiah Johnson
// CST-221
// Deadlock Assignment

#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <semaphore.h>
#include <time.h>


#define DATABASE_SIZE 10


struct Database {
  // Two tables that have information
  int tableA[DATABASE_SIZE];
  int tableB[DATABASE_SIZE];
};

int getRandomDbIndex() {
  // Get a random int in between 0 and the db size
  int i = rand() % (((DATABASE_SIZE - 1) + 1));
  printf("Random Number: %i\n", i);
  return i;
}

// Generates a random integer
int generateRandomInt() {
  return rand() % (100);
}

// Generates random values for "tables"
void generateRandomDBValues(struct Database * db) {
  for (int i = 0; i < DATABASE_SIZE; i++) {
    int rand_int = generateRandomInt();
    db - > tableA[i] = rand_int;
    rand_int = generateRandomInt();
    db - > tableB[i] = rand_int;
  }
}

// Writes to file when a deadlock occurs
void writeStatusToLog(char * input) {
  FILE * fp;

  fp = fopen("log.txt", "a");
  fputs(input, fp);
  fclose(fp);
}

int main() {
  int client1, client2;

  // Put db in shared memory so both processes can use it
  struct Database * db = mmap(NULL, sizeof(struct Database), PROT_READ | PROT_WRITE,
    MAP_SHARED | MAP_ANONYMOUS, -1, 0);

  // Mutexes to be used by both processes
  sem_t * mutexA = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE,
    MAP_SHARED | MAP_ANONYMOUS, -1, 0);

  sem_t * mutexB = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE,
    MAP_SHARED | MAP_ANONYMOUS, -1, 0);

  
  // Generate random values for the db tables
  generateRandomDBValues(db);

  // Initialize semaphores
  sem_init(mutexA, 0, 1);
  sem_init(mutexB, 0, 1);

  // Fork our two processes
  client1 = fork();
  client2 = fork();

  if (client1 < 0 || client2 < 0) {
    perror("forking error");
    exit(1);
  }

  // Client1 process
  if (client1 == 0) {
    int aValue = 0;

    while (1) {
      // Get time to be used by sem_timedwait function
      struct timespec ts;
      if (clock_gettime(CLOCK_REALTIME, & ts) == -1) {
        printf("couldnt get time\n");
      }

      // Equivalent to 5 second wait
      ts.tv_sec += 5;

      printf("Client 1 waiting on mutex A\n");

      //Wait on semaphores to be open
      int s = sem_timedwait(mutexA, & ts);
      printf("Client 1 mutex A: %i\n", s);
      printf("Client 1 got mutex A\n");
     
      // Called timed wait function to lock mutex B
      s = sem_timedwait(mutexB, & ts);
      
      if (s == -1) {
        // If timedwait fails, send to log, release mutex A so other process can use it 
        writeStatusToLog("Client 1 timed out trying to get mutexB\nClient 1 releasing mutexA...\n\n");
        sem_post(mutexA);
        sleep(2);
      } else {
        printf("Client 1 mutex B: %i\n", s);

        printf("Client 1 got mutex B\n");

        printf("\nClient 1 accessing table A...\n");

        //critical section 

        printf("Connection Established...\n");

        // Get a random index of the db to grab
        int rand_ind = getRandomDbIndex();

        // Retrieve the value at the random index
        aValue = db - > tableA[rand_ind];

        printf("Read [%i] from index [%i] in Database A\n", aValue, rand_ind);

        // set table B value to the value pulled from table A    
        db - > tableB[rand_ind] = aValue;

        printf("Closed DB connections...\n");

        // Print out the new database values
        printf("New database values: \n");
        for (int i = 0; i < DATABASE_SIZE; i++) {
          printf("%i ", db - > tableA[i]);
        }
        printf("\n");
        for (int i = 0; i < DATABASE_SIZE; i++) {
          printf("%i ", db - > tableB[i]);
        }
        printf("\n");

        sleep(3);
        printf("Client 1 releasing mutexes\n\n");
        // Release the mutexes
        sem_post(mutexB);
        sem_post(mutexA);
      }
    }
  }

  // Client 2 process
  if (client2 == 0) {
    int i = 0;

    int bValue = 0;

    while (1) {
      // Get time to be used by sem_timedwait function
      struct timespec ts;
      if (clock_gettime(CLOCK_REALTIME, & ts) == -1) {
        printf("couldnt get time\n");
      }

      // Equivalent to 5 second wait
      ts.tv_sec += 5;

      printf("Client 2 waiting on mutex B\n");

      //Wait on semaphores to be open
      int s = sem_timedwait(mutexB, & ts);

      printf("Client 2 mutex B: %i\n", s);
      printf("Client 2 got mutex B\n");

      // Called timed wait function to lock mutex A
      s = sem_timedwait(mutexA, & ts);

      if (s == -1) {
        // If timedwait fails, send to log, release mutex B so other process can use it 
        writeStatusToLog("Client 2 timed out trying to get mutexA\nClient 2 releasing mutexA...\n\n");
        sem_post(mutexB);
        sleep(2);
      } else {
        printf("Client 2 mutex A: %i\n", s);

        printf("Client 2 got mutex A\n");

        printf("\nClient 2 accessing table B...\n");

        //critical section 

        printf("Connection Established...\n");

        // Get a random index of the db to grab
        int rand_ind = getRandomDbIndex();

        // Retrieve the value at the random index
        bValue = db - > tableB[rand_ind];

        printf("Read [%i] from index [%i] in Database B\n", bValue, rand_ind);

        // set table B value to the value pulled from table A    
        db - > tableA[rand_ind] = bValue;

        printf("Closed DB connections...\n");

        // Print out the new database values
        printf("New database values: \n");
        for (int i = 0; i < DATABASE_SIZE; i++) {
          printf("%i ", db - > tableA[i]);
        }
        printf("\n");
        for (int i = 0; i < DATABASE_SIZE; i++) {
          printf("%i ", db - > tableB[i]);
        }
        printf("\n");

        sleep(3);
        printf("Client 2 releasing mutexes\n\n");
        // Release the mutexes
        sem_post(mutexB);
        sem_post(mutexA);
      }
    }
  }

  return 0;
}

#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#define DATABASE_SIZE 20

sem_t mutex;

int getRandomDbIndex();
int getRandomInt();

struct Database {
    // Data fields
    int dataFields[DATABASE_SIZE];

    // Is one connection established
    bool connectionEstablished;

    // ID of client_thread accessing database
    char client_id[5];
};

void* client1Thread(void* arg){
  struct Database* db = arg;
  char id[5] = "00001";

  while(1){
    //wait 
    sem_wait(&mutex); 
    printf("\nClient 1 accessing database...\n"); 
    
    //critical section 
    db->connectionEstablished = true;
    strcpy(db->client_id, id);
    printf("Connection Established...\n");

    int rand_ind = getRandomDbIndex();
    int rand_val = getRandomInt(20);
    db->dataFields[rand_ind] = rand_val;
    printf("Wrote [%i] to index [%i]\n", rand_val, rand_ind);

    db->connectionEstablished = false;
    strcpy(db->client_id, "00000");
    
    printf("Closed DB connection...\n");

    printf("New database values: \n");
    for(int i = 0; i <DATABASE_SIZE; i++){
        printf("%i ", db->dataFields[i]);
    }
    printf("\n\n");
      
    sem_post(&mutex);
    sleep(2);  
  }
     
}

void* client2Thread(void* arg){
  struct Database* db = arg;
  char id[5] = "00002";
  
  while(1){
    //wait 
    sem_wait(&mutex); 
    printf("\nClient 2 accessing database...\n"); 
    
    //critical section 
    db->connectionEstablished = true;
    strcpy(db->client_id, id);
    printf("Connection Established...\n");

    int rand_ind = getRandomDbIndex();
    int rand_val = getRandomInt(32);
    db->dataFields[rand_ind] = rand_val;
    printf("Wrote [%i] to index [%i]\n", rand_val, rand_ind);

    db->connectionEstablished = false;
    strcpy(db->client_id, "00000");
    
    printf("Closed DB connection...\n");
      
    printf("New database values: \n");
    for(int i = 0; i <DATABASE_SIZE; i++){
        printf("%i ", db->dataFields[i]);
    }
    printf("\n\n");

    sem_post(&mutex);
    sleep(2);  
  }
  
}

void* client3Thread(void* arg){
  struct Database* db = arg;
  char id[5] = "00003";
  
  while(1){
    //wait 
    sem_wait(&mutex); 
    printf("\nClient 3 accessing database...\n"); 
    
    //critical section 
    db->connectionEstablished = true;
    strcpy(db->client_id, id);
    printf("Connection Established...\n");

    int rand_ind = getRandomDbIndex();
    int rand_val = getRandomInt(11);
    db->dataFields[rand_ind] = rand_val;
    printf("Wrote [%i] to index [%i]\n", rand_val, rand_ind);

    db->connectionEstablished = false;
    strcpy(db->client_id, "00000");
    
    printf("Closed DB connection...\n");
    
    printf("New database values: \n");
    for(int i = 0; i <DATABASE_SIZE; i++){
        printf("%i ", db->dataFields[i]);
    }
    printf("\n\n");
  
    sem_post(&mutex);
    sleep(2);  
  }
}

void* client4Thread(void* arg){
  struct Database* db = arg;
  char id[5] = "00004";
  
  while(1){
    //wait 
    sem_wait(&mutex); 
    printf("\nClient 4 accessing database...\n"); 
    
    //critical section 
    db->connectionEstablished = true;
    strcpy(db->client_id, id);
    printf("Connection Established...\n");

    int rand_ind = getRandomDbIndex();
    int rand_val = getRandomInt(26);
    db->dataFields[rand_ind] = rand_val;
    printf("Wrote [%i] to index [%i]\n", rand_val, rand_ind);

    db->connectionEstablished = false;
    strcpy(db->client_id, "00000");
    
    printf("Closed DB connection...\n");
      
    printf("New database values: \n");
    for(int i = 0; i <DATABASE_SIZE; i++){
        printf("%i ", db->dataFields[i]);
    }
    printf("\n\n");

    sem_post(&mutex);
    sleep(2);  
  }
}


int getRandomDbIndex() 
{ 
    int i = rand() % (((DATABASE_SIZE-1) + 1));
    printf("Random Number: %i\n", i); 
    return i;
} 

int getRandomInt(int seed){
    srand(seed); 
    return rand() % (100);
}

int main(){
    struct Database db = {.dataFields = {10, 20, 24, 32, 26, 13, 72, 98, 100, 101}, .connectionEstablished = false, .client_id = "00000"};

    sem_init(&mutex, 0, 1);

    pthread_t client1, client2, client3, client4;
    

    pthread_create(&client1, NULL, client1Thread, &db); 
    pthread_create(&client2, NULL, client2Thread, &db); 
    pthread_create(&client3, NULL, client3Thread, &db); 
    pthread_create(&client4, NULL, client4Thread, &db); 

    printf("Created 4 client threads\n");

    pthread_join(client1, NULL); 
    pthread_join(client2, NULL); 
    pthread_join(client3, NULL); 
    pthread_join(client4, NULL); 

    printf("Joined threads\n");

    sem_destroy(&mutex);
    return 0;
}

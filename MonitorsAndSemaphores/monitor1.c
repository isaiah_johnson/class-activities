#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#define DATABASE_SIZE 20

int getRandomDbIndex();
int getRandomInt();
void makeTransaction();
int establishConnection();
int addToDB();
int disconnectFromDB();

struct Database {
    // Data fields
    int dataFields[DATABASE_SIZE];
  
    // ID of client_thread accessing database
    char connectedClient[5];
};

struct Monitor {
    // Is one connection established
    bool connectionEstablished;

    struct Database* db;

    // Function to establish connection
    void (*makeTransaction)(struct Database*, bool*, char[5]);
};


void makeTransaction(struct Database* db, bool* connectionEstablished, char clientId[5]){
  printf("Attempting Connection to DB from client '%s'\n", clientId);

  if(strcmp(db->connectedClient, clientId) == false){
    *connectionEstablished = true;
  }

  // If someone is already connected to the db, don't connect
  if(*connectionEstablished == true){
    printf("Connection to DB is already occupied by client '%s'\n\n", db->connectedClient) ;
    return;
  }
  else {
    printf("Client '%s' establishing connection to database\n", clientId);

    if(establishConnection(db, clientId, connectionEstablished) == 1){
      printf("Connection established!\n");

      printf("Adding value to database... \n");
      int index = getRandomDbIndex();
      int rand_val =  getRandomInt(time(NULL));

      if(addToDB(db, index, rand_val) == 1){
        printf("Value added to DB!\n");
      }
      else {
        printf("Could not add value to db!\n");
      }
      
      printf("New Database Values: ");
      for (int i = 0; i < DATABASE_SIZE; i++){
        printf("%i ", db->dataFields[i]);
      }
      printf("\n\n");
      sleep(5);

      if(disconnectFromDB(db, connectionEstablished) == 1){
        printf("Client '%s' Disconnected succesfully!\n\n", clientId);
      }
      else {
	printf("Couldn't disconnect from DB!\n");
      }
    }
    else {
      printf("Connection could not be established!\n");
    }

    
   
  }
}


int establishConnection(struct Database* db, char clientID[5], bool* connectionEstablished){
  strcpy(db->connectedClient, clientID);

  if(strcmp(db->connectedClient, clientID) == 0){
    *connectionEstablished = true;
    return 1;
  }
  else{
    *connectionEstablished = false;
    return 0;
  }
}

int addToDB(struct Database* db, int index, int value){
  db->dataFields[index] = value;

  if(db->dataFields[index] == value){
    return 1;
  }
  else {
    return 0;
  }
}

int disconnectFromDB(struct Database* db, bool* connectionEstablished) {
  *connectionEstablished = false;
  strcpy(db->connectedClient, "00000");

  if(strcmp(db->connectedClient, "00000") == 0){
    return 1;
  }
  else{
    return 0;
  }
}


void* client1Thread(void* arg){
  struct Monitor* mon = arg;
  while(1){
    mon->makeTransaction(mon->db, &mon->connectionEstablished, "00002");
    sleep(1);
  }
  
}

void* client2Thread(void* arg){
  struct Monitor* mon = arg;
  while(1){
    mon->makeTransaction(mon->db, &mon->connectionEstablished, "00002");
    sleep(2);
  }
  
}

void* client3Thread(void* arg){
  struct Monitor* mon = arg;
  while(1){
    mon->makeTransaction(mon->db, &mon->connectionEstablished, "00003");
    sleep(3);
  }
}

void* client4Thread(void* arg){
  struct Monitor* mon = arg;
  while(1){
    mon->makeTransaction(mon->db, &mon->connectionEstablished, "00004");
    sleep(4);
  }
}


int getRandomDbIndex() 
{ 
    int i = rand() % (((DATABASE_SIZE-1) + 1));
    return i;
} 

int getRandomInt(int seed){
    srand(seed); 
    return rand() % (100);
}

int main(){
    struct Monitor monitor = {.connectionEstablished = false};
    struct Database db = {.dataFields = {10, 20, 24, 32, 26, 13, 72, 98, 100, 101}, .connectedClient = "00000"};

    monitor.makeTransaction = makeTransaction;
    monitor.db = &db;


    pthread_t client1, client2, client3, client4;
    

    pthread_create(&client1, NULL, client1Thread, &monitor); 
    pthread_create(&client2, NULL, client2Thread, &monitor); 
    pthread_create(&client3, NULL, client3Thread, &monitor); 
    pthread_create(&client4, NULL, client4Thread, &monitor); 


    pthread_join(client1, NULL); 
    pthread_join(client2, NULL); 
    pthread_join(client3, NULL); 
    pthread_join(client4, NULL); 

    return 0;
}
